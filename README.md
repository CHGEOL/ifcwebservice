# IFCwebservice
Webservice to write non-CAD geometries to Industry Foundation Classes (IFC).

## Technology stack
- Debian 10
- Python 3.8
- Flask 2.0

### Dependencies
- [ifcopenshell](http://ifcopenshell.org/)
- [handsonifc](https://gitlab.com/geoquo/handsonifc)

## Features
Four different geometry and two sidecar readers.

### Geometry readers
- .DXF (excl. polyfaces)
- .OBJ
- .STL
- .TS

### Sidecar readers
- .JSON
- .XLSX