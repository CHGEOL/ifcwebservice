#####################
# handsonifc.reader #
#####################

from .dxf import DXFReader
from .json import JSONReader
from .obj import OBJReader
from .stl import STLReader
from .ts import TSReader
from .xlsx import XLSXReader


def get_geom_reader(ext):
    if ext == '.obj':
        return OBJReader
    elif ext == '.stl':
        return STLReader
    elif ext == '.dxf':
        return DXFReader
    elif ext == '.ts':
        return TSReader
    else:
        raise ValueError(f"could not find GeometryReader class for '{ext}'")


def get_sidecar_reader(ext):
    if ext == '.xlsx':
        return XLSXReader
    elif ext == '.json':
        return JSONReader
    else:
        raise ValueError(f"could not find SidecarReader class for '{ext}'")
